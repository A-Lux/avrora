window.$ = window.jQuery = require("jquery");
let owl_carousel = require('owl.carousel');
window.fn = owl_carousel;
require('popper.js');
require('bootstrap');
require('slick-carousel');


$(document).ready(function() {
    console.log('tooltip')
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $('.burger-menu-btn').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('burger-menu-lines-active');
        $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
        $('body').toggleClass('body-overflow');
    });

    let owlCompany = $('.owl-company')
    owlCompany.owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        autoplay:true,
        autoplayTimeout:1000,
        navSpeed: 1500,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })

ymaps.ready(init);

function init() {
  var myMap = new ymaps.Map("map", {
    center: [43.26141848, 76.94492184],
    zoom: 11
  }, {
    searchControlProvider: 'yandex#search'
  })
  var myPlacemark = new ymaps.Placemark(
    [43.26141848, 76.94492184]
  );
  myMap.geoObjects.add(myPlacemark);
}


$('.other-products-slider').slick({
    infinite: true,
    slidesToShow: 4,
    dots: true,
    nextArrow: '<img src="../images/arrow-next.png" alt="" class="slick-next-btn">',
    prevArrow: '<img src="../images/arrow-left.png" alt="" class="slick-prev-btn">',
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
  });
          

    $('.other-products-slider').slick({
        infinite: true,
        slidesToShow: 4,
        dots: true,
        nextArrow: '<img src="../images/arrow-next.png" alt="" class="slick-next-btn">',
        prevArrow: '<img src="../images/arrow-left.png" alt="" class="slick-prev-btn">',
    });
})

