<?php include('header.php') ?>


<div class="contacts">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <h3>Контакты</h3>
                <p><strong>Производственный комплекс «Аврора»</strong></p>

                <p>
                    <strong>Оптовые поставки:</strong>
                    +7 727 313 21 51 (вн 359)
                    <br>
                    +7 771 228 91 28
                </p>

                <p>
                    <strong>Тендерные продажи:</strong>

                    +7 727 313 11 88 (вн 303)
                    <br>
                    +7 705 900 08 44
                </p>

                <p>
                    <strong>Для дистрибьюторов:</strong>

                    +7 771 562 00 99
                    <br>
                    +7 727 313 11 88 (вн 326)
                </p>

                <p>
                    <strong> Контрактное производство:</strong>

                    +7 727 313 11 88 (вн 317)
                </p>

                <p>
                    <strong>WhatsApp:</strong>

                    +7 771 718 99 91
                </p>

                <p>
                    <strong>E-mail:</strong>

                    info@a-brands.kz
                </p>

                <p>
                    <strong>Адрес:</strong>

                    Республика Казахстан, 050060, г. Алматы, ул. Ходжанова, 79
                </p>

                <p>
                    <strong>Часы работы:</strong>

                    Пн-Пт 9-18
                    <br>
                    Сб, Вс - выходной
                </p>


            </div>
            <div class="col-xl-6">
                <a class="dg-widget-link"
                    href="http://2gis.kz/almaty/firm/70000001027755411/center/76.91292822360994,43.20917969039356/zoom/18?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть
                    на карте Алматы</a>
                <div class="dg-widget-link"><a
                        href="http://2gis.kz/almaty/firm/70000001027755411/photos/70000001027755411/center/76.91292822360994,43.20917969039356/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии
                        компании</a></div>
                <div class="dg-widget-link"><a
                        href="http://2gis.kz/almaty/center/76.912648,43.209042/zoom/18/routeTab/rsType/bus/to/76.912648,43.209042╎Аврора, бизнес-центр?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти
                        проезд до Аврора, бизнес-центр</a></div>
                <script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
                <script charset="utf-8">
                    new DGWidgetLoader({
                        "width": 640,
                        "height": 600,
                        "borderColor": "#a3a3a3",
                        "pos": {
                            "lat": 43.20917969039356,
                            "lon": 76.91292822360994,
                            "zoom": 18
                        },
                        "opt": {
                            "city": "almaty"
                        },
                        "org": [{
                            "id": "70000001027755411"
                        }]
                    });
                </script>
            </div>
            <hr>
        </div>
        <div class="contacts-bottom">
            <div class="title">
                <h4>Филиалы</h4>
            </div>
            <div class="row">
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Нур-Султан</p>
                    <p class="contact-text">Адрес: г. Нур-Султан, ул. Тархана, д 4, офис 322
                        <br>
                        Тел.: +7 771 779 0280
                        <br>
                        Тел.: +7 771 718 9803</p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Актобе</p>
                    <p class="contact-text">Адрес: г. Актобе, 11мкр, улица Аз-Наурыз 17,
                        <br>
                        БЦ «Асан», офис 208В.
                        <br>
                        Тел.: +7 771 779 0106
                        <br>
                        Тел.: +7 778 019 5028</p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">Алматинская область</p>
                    <p class="contact-text">
                        Дистрибьютор - ТОО «АСТРОН»
                        <br>
                        Адрес: г.Алматы, ул.Сүйінбай, д. 419
                        <br>
                        Тел.: +7 771 228 91 28
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Атырау</p>
                    <p class="contact-text">
                        Адрес: г. Атырау, проспект Султан Бейбарыс 15
                        <br>
                        Тел.: +7 701 374 1254
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Караганда</p>
                    <p class="contact-text">
                        Адрес: г. Караганда, ул.Гоголя 71, Офис "13"
                        <br>
                        Тел.: +7 721 250 5745
                        <br>
                        Тел.: +7 771 779 0280
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Костанай</p>
                    <p class="contact-text">
                        Дистрибьютер - ИП Кицик М.В.
                        <br>
                        Адрес: г. Кустанай, ул. Амангельды 25 офис 2,
                        <br>
                        Тел.: +7 7142 53 1616
                        <br>
                        Тел. +7 707 143 4989
                        <br>
                        WatsApp: +7 775 143 4989
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Петропавловск</p>
                    <p class="contact-text">
                        Дистрибьютор - ИП Симонова
                        <br>
                        Адрес: г. Петропавловск ул.Ауэзова 180
                        <br>
                        Тел.: +7 777 896 9546
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Семей</p>
                    <p class="contact-text">
                        Дистрибьютор - ТОО "YNITY"
                        <br>
                        Адрес: г.Семей, ул. Олега Кошевого, 30А
                        <br>
                        Тел.: +7 777 259 4579
                        <br>
                        Тел.: +7 777 996 9732
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Тараз</p>
                    <p class="contact-text">
                        Дистрибьютор - ТОО «ИнтерФуд-Тараз»
                        <br>
                        Адрес: г.Тараз, ул.Балуан Шолака, 1
                        <br>
                        Тел.: +7 707 556 3842
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Уральск</p>
                    <p class="contact-text">
                        Адрес: г. Уральск, ул.Оракбаева 4/3
                        <br>
                        Тел.: +7 777 791 5050
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Усть-Каменогорск</p>
                    <p class="contact-text">
                        Адрес: г.Усть-Каменогорск,
                        <br>
                        Проспект Назарбаева 8/1, офис 220
                        <br>
                        Тел.: +7 777 996 9732
                        <br>
                        Тел.: +7 777 155 3448
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Шымкент</p>
                    <p class="contact-text">
                        Адрес: г. Шымкент, улица Сайрамская 186/1, офис 4.
                        <br>
                        Тел.: +7 707 556 3842
                    </p>
                </div>
            </div>
        </div>
        <div class="contacts-bottom">
            <div class="title">
                <h4>Дистрибьюторы</h4>
            </div>
            <div class="row">
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Нур-Султан</p>
                    <p class="contact-text">Адрес: г. Нур-Султан, ул. Тархана, д 4, офис 322
                        <br>
                        Тел.: +7 771 779 0280
                        <br>
                        Тел.: +7 771 718 9803</p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Актобе</p>
                    <p class="contact-text">Адрес: г. Актобе, 11мкр, улица Аз-Наурыз 17,
                        <br>
                        БЦ «Асан», офис 208В.
                        <br>
                        Тел.: +7 771 779 0106
                        <br>
                        Тел.: +7 778 019 5028</p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">Алматинская область</p>
                    <p class="contact-text">
                        Дистрибьютор - ТОО «АСТРОН»
                        <br>
                        Адрес: г.Алматы, ул.Сүйінбай, д. 419
                        <br>
                        Тел.: +7 771 228 91 28
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Атырау</p>
                    <p class="contact-text">
                        Адрес: г. Атырау, проспект Султан Бейбарыс 15
                        <br>
                        Тел.: +7 701 374 1254
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Караганда</p>
                    <p class="contact-text">
                        Адрес: г. Караганда, ул.Гоголя 71, Офис "13"
                        <br>
                        Тел.: +7 721 250 5745
                        <br>
                        Тел.: +7 771 779 0280
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Костанай</p>
                    <p class="contact-text">
                        Дистрибьютер - ИП Кицик М.В.
                        <br>
                        Адрес: г. Кустанай, ул. Амангельды 25 офис 2,
                        <br>
                        Тел.: +7 7142 53 1616
                        <br>
                        Тел. +7 707 143 4989
                        <br>
                        WatsApp: +7 775 143 4989
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Петропавловск</p>
                    <p class="contact-text">
                        Дистрибьютор - ИП Симонова
                        <br>
                        Адрес: г. Петропавловск ул.Ауэзова 180
                        <br>
                        Тел.: +7 777 896 9546
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Семей</p>
                    <p class="contact-text">
                        Дистрибьютор - ТОО "YNITY"
                        <br>
                        Адрес: г.Семей, ул. Олега Кошевого, 30А
                        <br>
                        Тел.: +7 777 259 4579
                        <br>
                        Тел.: +7 777 996 9732
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Тараз</p>
                    <p class="contact-text">
                        Дистрибьютор - ТОО «ИнтерФуд-Тараз»
                        <br>
                        Адрес: г.Тараз, ул.Балуан Шолака, 1
                        <br>
                        Тел.: +7 707 556 3842
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Уральск</p>
                    <p class="contact-text">
                        Адрес: г. Уральск, ул.Оракбаева 4/3
                        <br>
                        Тел.: +7 777 791 5050
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Усть-Каменогорск</p>
                    <p class="contact-text">
                        Адрес: г.Усть-Каменогорск,
                        <br>
                        Проспект Назарбаева 8/1, офис 220
                        <br>
                        Тел.: +7 777 996 9732
                        <br>
                        Тел.: +7 777 155 3448
                    </p>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <p class="blue-text">г. Шымкент</p>
                    <p class="contact-text">
                        Адрес: г. Шымкент, улица Сайрамская 186/1, офис 4.
                        <br>
                        Тел.: +7 707 556 3842
                    </p>
                </div>
            </div>
        </div>
        <div class="contact-form">
            <h4 class="title">Напишите нам</h4>
            <form action="">
                <div class="row">
                    <div class="col-xl-6">
                        <label for="">Имя</label>
                        <input type="text">
                    </div>
                    <div class="col-xl-6">
                        <label for="">E-mail</label>
                        <input type="email">
                    </div>
                    <div class="col-xl-12">
                        <br>
                        <label for="">Сообщение</label>
                        <textarea name="" id="" rows="5"></textarea>
                    </div>
                </div>
                <input type="submit" name="Отправить">
            </form>
        </div>
    </div>
</div>







<?php include('footer.php') ?>