<?php include('header.php') ?>
<div class="catalog">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-xl-3 col-12 p-0">
                <button type="button" class="btn catalog-btn" data-toggle="modal" data-target="#catalog">
                    Продукция
                </button>
                <div class="modal fade" id="catalog" tabindex="-1" role="dialog" aria-labelledby="catalogTitle"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="catalogTitle">Продукция</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="catalog-left">
                                <div class="catalog-list">
                                    <a href="" class="category-title">Косметика</a>
                                    <span>По категориям</span>
                                    <a href="">Шампуни + Гели для душа</a>
                                    <a href=""> Шампунь - гель для душа «Горная лаванда»</a>
                                    <a href=""> Шампунь - Гель для душа «Райский сад»</a>
                                    <a href="">Жидкие мыла</a>
                                </div>
                                <div class="catalog-list">
                                    <span> По торговым маркам</span>
                                    <a href="">Avrora Cosmetics </a>
                                    <a href="">Мыльная опера </a>
                                    <a href="">WASHER-H </a>
                                    <a href="">AKMASEPT </a>
                                </div>
                                <div class="catalog-list">
                                    <a href="" class="category-title">Бытовая химия</a>
                                    <span>По категориям</span>
                                    <a href="">Средства для мытья посуды</a>
                                    <a href=""> Средства для мытья стёкол</a>
                                    <a href=""> Средства для кухни</a>
                                    <a href="">Средства для ванной, сантехники и унитазов</a>
                                    <a href="">Средства для мытья пола и стен</a>
                                    <a href="">Средства для чистки и стирки</a>
                                </div>
                                <div class="catalog-list">
                                    <span>По торговым маркам</span>
                                    <a href="">Effect </a>
                                    <a href="">Effect Professional </a>
                                    <a href="">Чистая кастрюлька </a>
                                    <a href="">WASHER </a>
                                </div>
                                <div class="catalog-list">
                                    <a href="" class="category-title">Гигиена</a>
                                    <span>По категориям</span>
                                    <a href="">Антисептики для рук</a>
                                    <a href="">Антисептики для ног</a>
                                </div>
                                <div class="catalog-list">
                                    <span>По торговым маркам</span>
                                    <a href="">AkmaSEPT </a>
                                </div>
                                <div class="catalog-list">
                                    <a href="" class="category-title">Параметры товара</a>
                                    <span>По категориям</span>
                                    <a href="">Серия бренда</a>
                                    <a href="">Наличие</a>
                                    <a href="">Характеристики</a>
                                    <a href="">Бренды косметики</a>
                                    <a href="">Бренды бытовой химии</a>
                                    <a href="">Бренды гигиены</a>
                                </div>
                                <div class="catalog-list">
                                    <span>По торговым маркам</span>
                                    <a href="">AkmaSEPT </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="catalog-left catalog-desktop">
                    <div class="catalog-list">
                        <a href="" class="category-title">Косметика</a>
                        <span>По категориям</span>
                        <a href="">Шампуни + Гели для душа</a>
                        <a href=""> Шампунь - гель для душа «Горная лаванда»</a>
                        <a href=""> Шампунь - Гель для душа «Райский сад»</a>
                        <a href="">Жидкие мыла</a>
                    </div>
                    <div class="catalog-list">
                        <span> По торговым маркам</span>
                        <a href="">Avrora Cosmetics </a>
                        <a href="">Мыльная опера </a>
                        <a href="">WASHER-H </a>
                        <a href="">AKMASEPT </a>
                    </div>
                    <div class="catalog-list">
                        <a href="" class="category-title">Бытовая химия</a>
                        <span>По категориям</span>
                        <a href="">Средства для мытья посуды</a>
                        <a href=""> Средства для мытья стёкол</a>
                        <a href=""> Средства для кухни</a>
                        <a href="">Средства для ванной, сантехники и унитазов</a>
                        <a href="">Средства для мытья пола и стен</a>
                        <a href="">Средства для чистки и стирки</a>
                    </div>
                    <div class="catalog-list">
                        <span>По торговым маркам</span>
                        <a href="">Effect </a>
                        <a href="">Effect Professional </a>
                        <a href="">Чистая кастрюлька </a>
                        <a href="">WASHER </a>
                    </div>
                    <div class="catalog-list">
                        <a href="" class="category-title">Гигиена</a>
                        <span>По категориям</span>
                        <a href="">Антисептики для рук</a>
                        <a href="">Антисептики для ног</a>
                    </div>
                    <div class="catalog-list">
                        <span>По торговым маркам</span>
                        <a href="">AkmaSEPT </a>
                    </div>
                    <div class="catalog-list">
                        <a href="" class="category-title">Параметры товара</a>
                        <span>По категориям</span>
                        <a href="">Серия бренда</a>
                        <a href="">Наличие</a>
                        <a href="">Характеристики</a>
                        <a href="">Бренды косметики</a>
                        <a href="">Бренды бытовой химии</a>
                        <a href="">Бренды гигиены</a>
                    </div>
                    <div class="catalog-list">
                        <span>По торговым маркам</span>
                        <a href="">AkmaSEPT </a>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 p-0">
                <div class="categories category-bg"
                    style='background-image:url(images/bg.jpg);background-position-x: 24%;background-size: cover;'>
                    <div class="categories-inner">
                        <h1>СРЕДСТВА ДЛЯ МЫТЬЯ ПОСУДЫ</h1>
                        <div class="row">
                            <div class="col-xl-3 col-12 col-md-6">
                                <a href="./product.php">
                                    <div class="category-product">
                                        <div class="category-image">
                                            <img src="images/product.png" alt="">
                                        </div>
                                </a>
                                <div class="category-text">
                                    <a href="./product.php">Средства для мытья посуды</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-12 col-md-6">
                            <a href="./product.php">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                            </a>
                            <div class="category-text">
                                <a href="./product.php">Средства для мытья посуды</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-12 col-md-6">
                        <a href="./product.php">
                            <div class="category-product">
                                <div class="category-image">
                                    <img src="images/product.png" alt="">
                                </div>
                        </a>
                        <div class="category-text">
                            <a href="./product.php">Средства для мытья посуды</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-12 col-md-6">
                    <a href="./product.php">
                        <div class="category-product">
                            <div class="category-image">
                                <img src="images/product.png" alt="">
                            </div>
                    </a>
                    <div class="category-text">
                        <a href="./product.php">Средства для мытья посуды</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<?php include('footer.php') ?>