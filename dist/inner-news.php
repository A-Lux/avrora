<?php include('header.php') ?>

<div class="container-fluid pl-3 pr-3">
    <div class="news-inner news">
        <div class="row">
            <div class="col-xl-2 col-lg-3">
                <div class="news-left">
                    <form action="">
                        <div class="search-block">
                            <div class="search ">
                                <div class="input-label">
                                    <input type="text" required>
                                    <label for="">Поиск...</label>
                                    <span class="bottom-line"></span>
                                </div>

                                <button class="search-btn">
                                <i class="fas fa-search"></i>
                                </button>
                            </div>

                        </div>
                    </form>
                    <div class="news-link">
                        <a href="#">Все</a>
                        <ul>
                            <li>
                                <a href="#">Новости компании</a>
                            </li>
                            <li>
                                <a href="#">СМИ о нас</a>
                            </li>
                            <li>
                                <a href="#">Новости рынка: события, мнения, тенденции</a>
                            </li>
                            <li>
                                <a href="#">Акции и розыгрыши</a>
                            </li>
                            <li>
                                <a href="#">Реклама</a>
                            </li>
                            <li>
                                <a href="#">Бгагодарности</a>
                            </li>
                        </ul>
                        <div class="archives">
                            <h5>Архивы</h5>
                            <select name="" id="">
                                <option value="">Выберите месяц</option>
                                <option value="">Март 2020</option>
                                <option value="">Февраль 2020</option>
                                <option value="">Май 2020</option>
                                <option value="">Апрель 2020</option>
                                <option value="">Ноябрь 2020</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-9">
                <div class=" col-xl-10 col-lg-10 m-auto pl-0 pr-0">
                    <div class="news-content">
                        <img src="./images/news-inner1.jpeg" alt="">
                        <img src="./images/news-inner2.jpeg" alt="">
                    </div>
                    <div class="news-inner-bottom">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="entry-categories">
                                    <p>Рубрики:</p>
                                    <span><a href="#">Все новости</a></span>
                                    <span><a href="#">Благодарности</a></span>
                                    <span><a href="#">Новости компании</a></span>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="icon-link">
                                    <a href="#" data-toggle="tooltip" data-placement="top" offset="5" title="Share on Facebook"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#" data-toggle="tooltip" data-placement="top" offset="5" title="Share on Twitter"><i class="fab fa-twitter"></i></a>
                                    <a href="#" data-toggle="tooltip" data-placement="top" offset="5" title="Share on Google"><i class="fab fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="news-comment">
                        <div class="title">
                            <h3>Добавить комментарий</h3>
                        </div>
                        <div>
                            <form action="">
                                <div class="form-input">
                                    <div class="input-label">
                                        <input type="text" required>
                                        <label for="">Имя*</label>
                                        <span class="bottom-line"></span>
                                    </div>
                                    <div class="input-label">
                                        <input type="text" required>
                                        <label for="">Эл.почта*</label>
                                        <span class="bottom-line"></span>
                                    </div>
                                    <div class="input-label">
                                        <input type="text" required>
                                        <label for="">Сайт*</label>
                                        <span class="bottom-line"></span>
                                    </div>
                                </div>
                                <div class="textarea-comment">
                                    <div class="input-label ">
                                        <textarea name="" id=""></textarea>
                                        <label for="">О чём хотите написать?</label>
                                        <span class="bottom-line"></span>
                                    </div>
                                    <div class="send-btn-block">
                                        <button class='send-btn'>Отправить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>





<?php include('footer.php') ?>