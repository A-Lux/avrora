<?php include('header.php') ?>
<div class="catalog">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-xl-3 col-12 p-0">
                <button type="button" class="btn catalog-btn" data-toggle="modal" data-target="#catalog">
                    Продукция
                </button>
                <div class="modal fade" id="catalog" tabindex="-1" role="dialog"
                    aria-labelledby="catalogTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="catalogTitle">Продукция</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="catalog-left">
                                <div class="catalog-list">
                                    <a href="./category.php" class="category-title">Косметика</a>
        
                                    <a href="./category.php" class='active-product'>Шампуни + Гели для душа</a>
                                    <a href="./category.php"> Шампунь - гель для душа «Горная лаванда»</a>
                                    <a href="./category.php"> Шампунь - Гель для душа «Райский сад»</a>
                                    <a href="./category.php">Жидкие мыла</a>
                                </div>
                                <div class="catalog-list">
                                    <a href="./category.php" class="category-title">Бытовая химия</a>
        
                                    <a href="./category.php">Средства для мытья посуды</a>
                                    <a href="./category.php"> Средства для мытья стёкол</a>
                                    <a href="./category.php"> Средства для кухни</a>
                                    <a href="./category.php">Средства для ванной, сантехники и унитазов</a>
                                    <a href="./category.php">Средства для мытья пола и стен</a>
                                    <a href="./category.php">Средства для чистки и стирки</a>
                                </div>
                                <div class="catalog-list">
                                    <a href="./category.php" class="category-title">Гигиена</a>
        
                                    <a href="./category.php">Антисептики для рук</a>
                                    <a href="./category.php">Антисептики для ног</a>
                                </div>
                                <div class="catalog-list">
                                    <span>Торговые марки</span>
                                    <a href="./category.php">AkmaSEPT </a>
                                    <a href="./category.php">Effect </a>
                                    <a href="./category.php">Effect Professional </a>
                                    <a href="./category.php">Чистая кастрюлька </a>
                                    <a href="./category.php">WASHER </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="catalog-left catalog-desktop">
                    <div class="catalog-list">
                        <p class="category-title" style='margin-bottom:0;'>Категории</p>
                        <a href="./category.php" class="category-title">Косметика</a>
                        <a href="./category.php" class='active-product'>Шампуни + Гели для душа</a>
                        <a href="./category.php"> Шампунь - гель для душа «Горная лаванда»</a>
                        <a href="./category.php"> Шампунь - Гель для душа «Райский сад»</a>
                        <a href="./category.php">Жидкие мыла</a>
                    </div>
                    <div class="catalog-list">
                        <a href="./category.php" class="category-title">Бытовая химия</a>
                        <a href="./category.php">Средства для мытья посуды</a>
                        <a href="./category.php"> Средства для мытья стёкол</a>
                        <a href="./category.php"> Средства для кухни</a>
                        <a href="./category.php">Средства для ванной, сантехники и унитазов</a>
                        <a href="./category.php">Средства для мытья пола и стен</a>
                        <a href="./category.php">Средства для чистки и стирки</a>
                    </div>
                    <div class="catalog-list">
                        <a href="./category.php" class="category-title">Гигиена</a>
                        <a href="./category.php">Антисептики для рук</a>
                        <a href="./category.php">Антисептики для ног</a>
                    </div>
                    <div class="catalog-list">
                        <a href="./category.php" class="category-title">Торговые марки</a>
                        <a href="./category.php">Avrora Cosmetics </a>
                        <a href="./category.php">Мыльная опера </a>
                        <a href="./category.php">WASHER-H </a>
                        <a href="./category.php">AKMASEPT </a>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 p-0">
                <div class="categories">
                    <div class="categories-inner">
                        <h1>БЫТОВАЯ ХИМИЯ</h1>
                        <div class="row">
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="categories-inner">
                        <h1>ГИГИЕНА</h1>
                        <div class="row">
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="categories-inner">
                        <h1>КОСМЕТИКА</h1>
                        <div class="row">
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="categories-inner categories-inner-bottom">
                        <h1>ТОРГОВЫЕ МАРКИ </h1>
                        <div class="row">
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12 col-md-6">
                                <div class="category-product">
                                    <div class="category-image">
                                        <img src="images/product.png" alt="">
                                    </div>
                                    <div class="category-text">
                                        <a href="">Средства для мытья посуды</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php') ?>