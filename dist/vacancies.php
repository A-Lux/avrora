<?php include('header.php') ?>
<div class="container">
    <div class="vacancies">
        <div class="title">
            <h4>Вакансии</h4>
        </div>
        <div class="vacancies-content">
            <p>Вакансии</p>
            <ul>
                <li>
                    <a href="#">JavaScript разработчик</a>
                    <span>от 400000 KZT</span>
                </li>
                <li>
                    <a href="#">Senior / Middle Python разработчик</a>
                    <span>от 400000 KZT</span>
                </li>
                <li>
                    <a href="#">
                        Грузчик
                    </a>
                    <span>от 140000 KZT</span>
                </li>
                <li>
                    <a href="#">Специалист финансового отдела</a>
                    <span>от 100000 KZT</span>
                </li>
                <li>
                    <a href="#">
                        Грузчик
                    </a>
                    <span>от 120000 KZT</span>
                </li>
                <li>
                    <a href="#">Менеджер отдела логистики/снабжения</a>
                </li>
                <li>
                    <a href="#">
                        <img src="./images/hh.png" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>


<?php include('footer.php') ?>