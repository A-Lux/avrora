<?php include('header.php') ?>
    
<div class="main">
        <div class="slider-wrapper">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="slide-item"><img src="images/slide_01.jpg" alt=""></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="slide-item"><img src="images/slide_01.jpg" alt=""></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="slide-item"><img src="images/slide_01.jpg" alt=""></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="slide-item"><img src="images/slide_01.jpg" alt=""></div>
                    </div>
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-pagination"></div>
            </div>
            <div class="category"><a class="category__item" href="category.php">
                    <div class="title">Косметика</div>
                    <div class="undertitle">Средства для ухода за волосами и телом</div>
                </a><a class="category__item" href="category.php">
                    <div class="title">Бытовая химия</div>
                    <div class="undertitle">Стредства для ухода за домом</div>
                </a><a class="category__item" href="category.php">
                    <div class="title">Гигиена</div>
                    <div class="undertitle">Средства личной гигиены</div>
                </a></div>
        </div>
    </div>

<?php include('footer.php') ?>
