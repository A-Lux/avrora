<?php include('header.php') ?>
<div class="container-fluid pl-3 pr-3">
    <div class="news">
        <div class="row">
            <div class="col-xl-2 col-lg-3">
                <div class="news-left">
                    <form action="">
                        <div class="search-block">
                            <div class="search">
                                <div class="input-label">
                                    <input type="text" required>
                                    <label for="">Поиск...</label>
                                    <span class="bottom-line"></span>
                                </div>
                                <button class="search-btn">
                                    <i class="fas fa-search"></i>

                                </button>
                            </div>

                        </div>
                    </form>
                    <div class="news-link">
                        <a href="#">Все</a>
                        <ul>
                            <li>
                                <a href="#">Новости компании</a>
                            </li>
                            <li>
                                <a href="#">СМИ о нас</a>
                            </li>
                            <li>
                                <a href="#">Новости рынка: события, мнения, тенденции</a>
                            </li>
                            <li>
                                <a href="#">Акции и розыгрыши</a>
                            </li>
                            <li>
                                <a href="#">Реклама</a>
                            </li>
                            <li>
                                <a href="#">Бгагодарности</a>
                            </li>
                        </ul>
                        <div class="archives">
                            <h5>Архивы</h5>
                            <select name="" id="">
                                <option value="">Выберите месяц</option>
                                <option value="">Март 2020</option>
                                <option value="">Февраль 2020</option>
                                <option value="">Май 2020</option>
                                <option value="">Апрель 2020</option>
                                <option value="">Ноябрь 2020</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-9">
                <p class="search-info">
                Извините, по вашему запросу ничего не найдено. Попробуйте другие ключевые слова.
                </p>
            <form action="">
                        <div class="search-block">
                            <div class="search search-page">
                                <div class="input-label">
                                    <input type="text" required="">
                                    <label for="">Поиск...</label>
                                    <span class="bottom-line"></span>
                                </div>
                                <button class="search-btn">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>

                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>