<?php include('header.php') ?>
<div class="catalog product">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-xl-3 col-12 p-0">
                <button type="button" class="btn catalog-btn" data-toggle="modal" data-target="#catalog">
                    Продукция
                </button>
                <div class="modal fade" id="catalog" tabindex="-1" role="dialog" aria-labelledby="catalogTitle"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="catalogTitle">Продукция</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="catalog-left">
                                <div class="catalog-list">
                                    <a href="./category.php" class="category-title">Косметика</a>
                                    <span>По категориям</span>
                                    <a href="./category.php">Шампуни + Гели для душа</a>
                                    <a href="./category.php"> Шампунь - гель для душа «Горная лаванда»</a>
                                    <a href="./category.php"> Шампунь - Гель для душа «Райский сад»</a>
                                    <a href="./category.php">Жидкие мыла</a>
                                </div>
                                <div class="catalog-list">
                                    <span> По торговым маркам</span>
                                    <a href="./category.php">Avrora Cosmetics </a>
                                    <a href="./category.php">Мыльная опера </a>
                                    <a href="./category.php">WASHER-H </a>
                                    <a href="./category.php">AKMASEPT </a>
                                </div>
                                <div class="catalog-list">
                                    <a href="./category.php" class="category-title">Бытовая химия</a>
                                    <span>По категориям</span>
                                    <a href="./category.php">Средства для мытья посуды</a>
                                    <a href="./category.php"> Средства для мытья стёкол</a>
                                    <a href="./category.php"> Средства для кухни</a>
                                    <a href="./category.php">Средства для ванной, сантехники и унитазов</a>
                                    <a href="./category.php">Средства для мытья пола и стен</a>
                                    <a href="./category.php">Средства для чистки и стирки</a>
                                </div>
                                <div class="catalog-list">
                                    <span>По торговым маркам</span>
                                    <a href="./category.php">Effect </a>
                                    <a href="./category.php">Effect Professional </a>
                                    <a href="./category.php">Чистая кастрюлька </a>
                                    <a href="./category.php">WASHER </a>
                                </div>
                                <div class="catalog-list">
                                    <a href="./category.php" class="category-title">Гигиена</a>
                                    <span>По категориям</span>
                                    <a href="./category.php">Антисептики для рук</a>
                                    <a href="./category.php">Антисептики для ног</a>
                                </div>
                                <div class="catalog-list">
                                    <span>По торговым маркам</span>
                                    <a href="./category.php">AkmaSEPT </a>
                                </div>
                                <div class="catalog-list">
                                    <a href="./category.php" class="category-title">Параметры товара</a>
                                    <span>По категориям</span>
                                    <a href="./category.php">Серия бренда</a>
                                    <a href="./category.php">Наличие</a>
                                    <a href="./category.php">Характеристики</a>
                                    <a href="./category.php">Бренды косметики</a>
                                    <a href="./category.php">Бренды бытовой химии</a>
                                    <a href="./category.php">Бренды гигиены</a>
                                </div>
                                <div class="catalog-list">
                                    <span>По торговым маркам</span>
                                    <a href="./category.php">AkmaSEPT </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="catalog-left catalog-desktop">
                    <div class="catalog-list">
                        <a href="./category.php" class="category-title">Косметика</a>
                        <span>По категориям</span>
                        <a href="./category.php">Шампуни + Гели для душа</a>
                        <a href="./category.php"> Шампунь - гель для душа «Горная лаванда»</a>
                        <a href="./category.php"> Шампунь - Гель для душа «Райский сад»</a>
                        <a href="./category.php">Жидкие мыла</a>
                    </div>
                    <div class="catalog-list">
                        <span> По торговым маркам</span>
                        <a href="./category.php">Avrora Cosmetics </a>
                        <a href="./category.php">Мыльная опера </a>
                        <a href="./category.php">WASHER-H </a>
                        <a href="./category.php">AKMASEPT </a>
                    </div>
                    <div class="catalog-list">
                        <a href="./category.php" class="category-title">Бытовая химия</a>
                        <span>По категориям</span>
                        <a href="./category.php">Средства для мытья посуды</a>
                        <a href="./category.php"> Средства для мытья стёкол</a>
                        <a href="./category.php"> Средства для кухни</a>
                        <a href="./category.php">Средства для ванной, сантехники и унитазов</a>
                        <a href="./category.php">Средства для мытья пола и стен</a>
                        <a href="./category.php">Средства для чистки и стирки</a>
                    </div>
                    <div class="catalog-list">
                        <span>По торговым маркам</span>
                        <a href="./category.php">Effect </a>
                        <a href="./category.php">Effect Professional </a>
                        <a href="./category.php">Чистая кастрюлька </a>
                        <a href="./category.php">WASHER </a>
                    </div>
                    <div class="catalog-list">
                        <a href="./category.php" class="category-title">Гигиена</a>
                        <span>По категориям</span>
                        <a href="./category.php">Антисептики для рук</a>
                        <a href="./category.php">Антисептики для ног</a>
                    </div>
                    <div class="catalog-list">
                        <span>По торговым маркам</span>
                        <a href="./category.php">AkmaSEPT </a>
                    </div>
                    <div class="catalog-list">
                        <a href="./category.php" class="category-title">Параметры товара</a>
                        <span>По категориям</span>
                        <a href="./category.php">Серия бренда</a>
                        <a href="./category.php">Наличие</a>
                        <a href="./category.php">Характеристики</a>
                        <a href="./category.php">Бренды косметики</a>
                        <a href="./category.php">Бренды бытовой химии</a>
                        <a href="./category.php">Бренды гигиены</a>
                    </div>
                    <div class="catalog-list">
                        <span>По торговым маркам</span>
                        <a href="./category.php">AkmaSEPT </a>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 p-0">
                <div class="categories category-bg"
                    style='background-image:url(images/bg-product.jpg);background-position-x: 24%;background-size: cover;'>
                    <div class="categories-inner product-inner">
                        <div class="row product-row">
                            <div class="col-xl-5 col-md-5">
                                <div class="product-image">
                                    <img src="images/product-1.png" alt="">
                                    <div class="product-sub">
                                        <img src="images/products-circle.png" alt="">
                                        <span>Чистота и здоровье</span>
                                    </div>
                                    <p>Артикул: 638, объем: 370 мл</p>
                                </div>
                            </div>
                            <div class="col-xl-7 p-0 col-md-7">
                                <div class="product-content">
                                    <h1>Антибактериальное жидкое мыло «Aloe Vera»</h1>
                                    <p>Торговая марка: «AKMASEPT»</p>
                                    <p><img src="images/product-icon.png" alt="">с антибактериальным эффектом</p>
                                    <div class="list-group" id="list-tab" role="tablist">
                                        <a class="list-group-item list-group-item-action active" id="about-list"
                                            data-toggle="list" href="#about" role="tab"
                                            aria-controls="home">Описание</a>
                                        <a class="list-group-item list-group-item-action" id="application-list"
                                            data-toggle="list" href="#application" role="tab"
                                            aria-controls="profile">Способ применения</a>
                                        <a class="list-group-item list-group-item-action" id="structure-list"
                                            data-toggle="list" href="#structure" role="tab"
                                            aria-controls="messages">Состав</a>
                                    </div>
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="about" role="tabpanel"
                                            aria-labelledby="about-list">Мыло “AKMASEPT” подходит для всех типов кожи и
                                            для ежедневного использования. Образует нежную пену, бережно и эффективно
                                            заботится
                                            о руках, очищая и сохраняя их мягкость. В составе содержит специальный
                                            антибактериальный компонент, убивающий вредные бактерии. Удобная форма
                                            флакона с дозатором обеспечит гигиеничность и экономность в расходовании
                                            средства. Не содержит карсителей.</div>
                                        <div class="tab-pane fade" id="application" role="tabpanel"
                                            aria-labelledby="application-list">Мыло “AKMASEPT” подходит для всех типов
                                            кожи и для ежедневного использования. Образует нежную пену, бережно и
                                            эффективно заботится
                                            о руках, очищая и сохраняя их мягкость. В составе содержит специальный
                                            антибактериальный компонент, убивающий вредные бактерии. Удобная форма
                                            флакона с дозатором обеспечит гигиеничность и экономность в расходовании
                                            средства. Не содержит карсителей.</div>
                                        <div class="tab-pane fade" id="structure" role="tabpanel"
                                            aria-labelledby="structure-list">Мыло “AKMASEPT” подходит для всех типов
                                            кожи и для ежедневного использования. Образует нежную пену, бережно и
                                            эффективно заботится
                                            о руках, очищая и сохраняя их мягкость. В составе содержит специальный
                                            антибактериальный компонент, убивающий вредные бактерии. Удобная форма
                                            флакона с дозатором обеспечит гигиеничность и экономность в расходовании
                                            средства. Не содержит карсителей.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="other-products">
                            <h1>ДРУГИЕ ТОВАРЫ КАТЕГОРИИ ЖИДКИЕ МЫЛА</h1>
                            <div class="other-products-slider">
                                <div class="product-slide">
                                    <div class="category-product"><a href="">
                                        <div class="category-image">
                                            <img src="images/product.png" alt="">
                                        </div>
                                </a>
                                <div class="category-text">
                                    <a href="">Средства для мытья посуды</a>
                                </div>
                            </div>
                                </div>
                                <div class="product-slide">
                                    <div class="category-product"><a href="">
                                        <div class="category-image">
                                            <img src="images/product.png" alt="">
                                        </div>
                                </a>
                                <div class="category-text">
                                    <a href="">Средства для мытья посуды</a>
                                </div>
                            </div>
                                </div>
                                <div class="product-slide">
                                    <div class="category-product"><a href="">
                                        <div class="category-image">
                                            <img src="images/product.png" alt="">
                                        </div>
                                </a>
                                <div class="category-text">
                                    <a href="">Средства для мытья посуды</a>
                                </div>
                            </div>
                                </div>
                                <div class="product-slide">
                                    <div class="category-product"><a href="">
                                        <div class="category-image">
                                            <img src="images/product.png" alt="">
                                        </div>
                                </a>
                                <div class="category-text">
                                    <a href="">Средства для мытья посуды</a>
                                </div>
                            </div>
                                </div>
                                <div class="product-slide">
                                    <div class="category-product"><a href="">
                                        <div class="category-image">
                                            <img src="images/product.png" alt="">
                                        </div>
                                </a>
                                <div class="category-text">
                                    <a href="">Средства для мытья посуды</a>
                                </div>
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<?php include('footer.php') ?>