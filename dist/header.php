<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/style.min.css">
</head>
<body>
    <header class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8">
                    <nav class="header-links">
                        <li>
                            <a href="" class='active-link'><img src="images/home-image.png" class="home-image" alt="">ГЛАВНАЯ</a>
                        </li>
                        <li>
                            <a href="./catalog.php">ПРОДУКЦИЯ</a>
                        </li>
                        <li>
                            <a href="./about-company.php">О КОМПАНИИ</a>
                        </li>
                        <li>
                            <a href="./news.php">НОВОСТИ</a>
                        </li>
                        <li>
                            <a href="./where-buy.php">ГДЕ КУПИТЬ</a>
                        </li>
                        <li>
                            <a href="./vacancies.php">СОЦИАЛЬНАЯ ДЕЯТЕЛЬНОСТЬ</a>
                        </li>
                        <li>
                            <a href="./contacts.php">КОНТАКТЫ</a>
                        </li>
                    </nav>
                </div>
                <div class="col-xl-2">
                    <div class="language">
                        <a href="">KZ</a>
                        <a href="" class="lang-active">RU</a>
                        <a href="">EN</a>
                    </div>
                </div>
                <div class="col-xl-2">
                    <div class="logo">
                        <a href=""><img src="images/logo.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="burger-menu-image">
            <a href="index.php">
                <img src="images/logo.png" alt="">
            </a>
        </div>
        <div class="burger-menu-content">
            <a href="#" class="burger-menu-btn">
                <span class="burger-menu-lines"></span>
            </a>
        </div>
        <div class="nav-panel-mobil">
            <div class="container">
                <nav class="navbar-expand-lg navbar-light">
                    <ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
                        <li class="nav-item">
                            <a href=""><img src="images/home-image.png" class="home-image" alt="">ГЛАВНАЯ</a>
                        </li>
                        <li class="nav-item">
                            <a href="">ПРОДУКЦИЯ</a>
                        </li>
                        <li class="nav-item">
                            <a href="">О КОМПАНИИ</a>
                        </li>
                        <li class="nav-item">
                            <a href="">НОВОСТИ</a>
                        </li>
                        <li class="nav-item">
                            <a href="">ГДЕ КУПИТЬ</a>
                        </li>
                        <li class="nav-item">
                            <a href="">СОЦИАЛЬНАЯ ДЕЯТЕЛЬНОСТЬ</a>
                        </li>
                        <li class="nav-item">
                            <a href="">КОНТАКТЫ</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

    </header>

</body>
</html>