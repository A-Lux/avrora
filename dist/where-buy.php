<?php include('header.php') ?>
<div class="where-buy">
    <div class="container-fluid">
        <h1>Где купить</h1>
        <div class="row">
            <div class="col-xl-6 col-md-6">
                <div class="buy-left">
                    <div class="city-select">
                        <select>
                            <option value="">Выберите свой город:</option>
                            <option value="">Все города</option>
                            <option value="">Алматы</option>
                            <option value="">Астана</option>
                            <option value="">Актобе</option>
                            <option value="">Уральск</option>
                            <option value="">Караганда</option>
                            <option value="">Павлодар</option>
                            <option value="">Шымкент</option>
                            <option value="">Экибастуз</option>
                        </select>
                    </div>
                    <div class="brands-wrapper">
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                        <div class="brands-item">
                            <a href="">
                                <img src="images/carefood.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<?php include('footer.php') ?>