<div class="footer">
    <div class="container">
       <div class="row">
        <div class="col-xl-9 col-12 col-md-8">
            <div class="copyright">
                <p>© Copyright 2005-2020. Производственный комплекс "Аврора". <br>
                    Производство и оптовые поставки бытовой химии, косметики и средств гигиены.</p>
            </div>
        </div>
        <div class="col-xl-3 col-12 col-md-4">
            <div class="footer-links">
                <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
            </div>
        </div>
       </div>
    </div>
</div>
<script src="https://api-maps.yandex.ru/2.1/?&lang=ru_RU" type="text/javascript"></script>
<script src="js/main.js"></script>
<script src="js/main.min.js"></script>
</body>
</html>