<?php include('header.php') ?>
<div class="container-fluid pl-3 pr-3">
    <div class="news">
        <div class="row">
            <div class="col-xl-2 col-lg-3">
                <div class="news-left">
                    <form action="">
                        <div class="search-block">
                            <div class="search">
                                <div class="input-label">
                                    <input type="text" required>
                                    <label for="">Поиск...</label>
                                    <span class="bottom-line"></span>
                                </div>
                                <button class="search-btn">
                                    <i class="fas fa-search"></i>

                                </button>
                            </div>

                        </div>
                    </form>
                    <div class="news-link">
                        <a href="#">Все</a>
                        <ul>
                            <li>
                                <a href="#">Новости компании</a>
                            </li>
                            <li>
                                <a href="#">Новости рынка</a>
                            </li>
                            <li>
                                <a href="#">Акции и розыгрыши</a>
                            </li>
                            <li>
                                <a href="#">СМИ о нас</a>
                            </li>
                            <li>
                                <a href="#">Реклама</a>
                            </li>
                            <li>
                                <a href="#">Бгагодарности</a>
                            </li>
                        </ul>
                        <div class="archives">
                            <select name="" id="">
                                <option value="">Выберите год</option>
                                <option value="">2020</option>
                                <option value="">2019</option>
                                <option value="">2018</option>
                                <option value="">2017</option>
                                <option value="">2016</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-9">
                <div class="title">
                    <h2>Новости</h2>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news1.jpg" alt="">
                            <a href="./inner-news.php">Как избежать заражения коронавирусом?</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news2.jpg" alt="">
                            <a href="./inner-news.php">Производственный комплекс «Аврора» с торговой маркой «AKMASEPT» – технический спонсор «Kúzdik Jarty Marathon»</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news3.jpg" alt="">
                            <a href="./inner-news.php">Итоги Акции «Творим добро вместе!»</a>
                            <div class="news-card-link">
                                <span>24.01.2020 </span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news4.jpg" alt="">
                            <a href="#">99,9 % — рекламная уловка или реальная эффективность?.. Карманные антисептики</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news1.jpg" alt="">
                            <a href="#">Как избежать заражения коронавирусом?</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news2.jpg" alt="">
                            <a href="#">Производственный комплекс «Аврора» с торговой маркой «AKMASEPT» – технический спонсор «Kúzdik Jarty Marathon»</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news3.jpg" alt="">
                            <a href="#">Итоги Акции «Творим добро вместе!»</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news4.jpg" alt="">
                            <a href="#">99,9 % — рекламная уловка или реальная эффективность?.. Карманные антисептики</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news1.jpg" alt="">
                            <a href="#">Как избежать заражения коронавирусом?</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news2.jpg" alt="">
                            <a href="#">Производственный комплекс «Аврора» с торговой маркой «AKMASEPT» – технический спонсор «Kúzdik Jarty Marathon»</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news3.jpg" alt="">
                            <a href="#">Итоги Акции «Творим добро вместе!»</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news4.jpg" alt="">
                            <a href="#">99,9 % — рекламная уловка или реальная эффективность?.. Карманные антисептики</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news1.jpg" alt="">
                            <a href="#">Как избежать заражения коронавирусом?</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news2.jpg" alt="">
                            <a href="#">Производственный комплекс «Аврора» с торговой маркой «AKMASEPT» – технический спонсор «Kúzdik Jarty Marathon»</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news3.jpg" alt="">
                            <a href="#">Итоги Акции «Творим добро вместе!»</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="news-card">
                            <img src="./images/news4.jpg" alt="">
                            <a href="#">99,9 % — рекламная уловка или реальная эффективность?.. Карманные антисептики</a>
                            <div class="news-card-link">
                                <span>24.01.2020</span>
                                <span class="line"> / </span>
                                <a href="">Все новости,</a>
                                <a href="">Новости компании,</a>
                                <a href="">Новости рынка:</a>
                                <a href="">события,</a>
                                <a href="">мнения,</a>
                                <a href="">тенденции</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>